const base = (process.env.NODE_ENV == 'production') ? '/' : '/'
// const base = '/'

module.exports = {
  mode: 'spa',
  generate: {
    subFolders: true
  },
  isDev: (process.env.NODE_ENV !== 'production'),
  transition: {
    name: 'fadeOpacity',
    mode: 'out-in'
  },
  head: {
    title: 'Colégio Aparecida NP',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'msapplication-TileImage', content: base + 'ms-icon-144x144png' },
      { name: 'theme-color', content: '#ffffff' }
    ],
    link: [
      // { rel: 'icon', type: 'image/x-icon', href: 'http://aparecidanp.com.br/novo/images/favicon.png' },

      { rel: 'apple-touch-icon', sizes: '57x57', href: base + 'apple-icon-57x57.png' },
      { rel: 'apple-touch-icon', sizes: '60x60', href: base + 'apple-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes: '72x72', href: base + 'apple-icon-72x72.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: base + 'apple-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes: '114x114', href: base + 'apple-icon-114x114.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: base + 'apple-icon-120x120.png' },
      { rel: 'apple-touch-icon', sizes: '144x144', href: base + 'apple-icon-144x144.png' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: base + 'apple-icon-152x152.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: base + 'apple-icon-180x180.png' },
      { rel: 'icon', type: 'image/png', sizes: '192x192',  href: base + 'android-icon-192x192.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: base + 'favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '96x96', href: base + 'favicon-96x96.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: base + 'favicon-16x16.png' },
      { rel: 'manifest', href: base + 'manifest.json' },

      { rel: 'stylesheet', href: base + 'css/bootstrap.min.css' },
      { href: 'https://fonts.googleapis.com/css?family=Lora:400,700', rel: 'stylesheet' },
      { href: 'https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:300,400,500,700,900', rel: 'stylesheet' },
      { rel: 'stylesheet', href: base + 'css/font-awesome.min.css' },
      { rel: 'stylesheet', href: base + 'css/simple-line-icons.css' },
      { rel: 'stylesheet', href: base + 'css/slick.css' },
      { rel: 'stylesheet', href: base + 'css/slick-theme.css' },
      { rel: 'stylesheet', href: base + 'css/owl.carousel.min.css' },
      { href: base + 'css/style.css', rel: 'stylesheet' }
    ],
    script: [
      { src: base + 'js/jquery.min.js' },
      { src: base + 'js/tether.min.js' },
      { src: base + 'js/bootstrap.min.js' },
      { src: base + 'js/slick.min.js' },
      { src: base + 'js/waypoints.min.js' },
      { src: base + 'js/counterup.min.js' },
      { src: base + 'js/instafeed.min.js' },
      { src: base + 'js/owl.carousel.min.js' },
      { src: base + 'js/validate.js' },
      { src: base + 'js/tweetie.min.js' },
      { src: base + 'js/subscribe.js' },
      { src: base + 'js/script.js' }
    ]
  },
  loading: { color: '#3B8070' },
  build: {
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  plugins: [
    '~/plugins/vue-pure-lightbox',
    '~/plugins/vue-moment'
  ],
  env: {
    baseUrl: 'http://aparecidanp.com.br/',
    // apiUrl: 'http://aparecidanp.com.br/novo/api/'
    apiUrl: 'http://aparecidanp.com.br/adm/api/'
  },
  router: {
    base: base
  }
}

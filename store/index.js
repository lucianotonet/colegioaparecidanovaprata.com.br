import Vuex from 'vuex'
import axios from 'axios'
// import _ from 'lodash'

const createStore = () => {
  return new Vuex.Store({
    state,
    mutations,
    actions,
    getters
  })
}

export const state = {
  menu: {},
  currentPage: {},
  homedestaque: {},
  homenoticias: [],
  informacoes: {},
  loading: false,
  slider: [],
  maintenancemode: false,
  sidebar: {
    posts: []
  }
}

export const mutations = {
  FETCH_MENU (state, payload) {
    state.menu = payload
  },
  FETCH_HOME_DESTAQUE (state, payload) {
    state.homedestaque = payload
  },
  FETCH_HOME_NOTICIAS (state, payload) {
    state.homenoticias = payload
  },
  FETCH_PAGE (state, payload) {
    state.currentPage = payload
  },
  FETCH_SLIDER (state, payload) {
    state.slider = payload
  },
  FETCH_INFORMACOES (state, payload) {
    state.informacoes = payload
  },
  SET_LOADING (state, payload = true) {
    state.loading = payload
  },
  SET_MAINTENANCE_MODE (state, payload = true) {
    state.maintenancemode = payload
  },
  SET_SIDEBAR_POSTS (state, payload) {
    state.sidebar.posts = payload
  }
}

export const actions = {
  // nuxtServerInit ({ dispatch }, req) {
  //   dispatch('fetchMenu')
  //   dispatch('fetchHomeDestaque')
  //   dispatch('fetchHomeNoticias')
  //   dispatch('fetchHomeInfo')
  //   if (req.id) {
  //     dispatch('fetchPage', req)
  //   }
  // },
  async fetchHomeDestaque (store) {
    const { data } = await axios.get(process.env.apiUrl + '/')
    store.commit('FETCH_HOME_DESTAQUE', data.destaque)
  },
  async fetchHomeNoticias (store) {
    const { data } = await axios.get(process.env.apiUrl + '/')
    store.commit('FETCH_HOME_NOTICIAS', data.noticias)
  },
  async fetchMenu (store) {
    const { data } = await axios.get(process.env.apiUrl + '/')
    store.commit('FETCH_MENU', data.menu)
  },
  async fetchHomeSlider (store) {
    const { data } = await axios.get(process.env.apiUrl + '/')
    store.commit('FETCH_SLIDER', data.slider)
  },
  async fetchHomeInfo (store) {
    const { data } = await axios.get(process.env.apiUrl + '/')
    store.commit('FETCH_INFORMACOES', data.informacoes)
  },
  async fetchPage (store, page) {
    store.commit('FETCH_PAGE', null)
    var paramId = page.id ? '/?id=' + page.id : '/'

    const { data } = await axios.get(process.env.apiUrl + '/' + page.name + paramId)
    store.commit('FETCH_PAGE', data)
  },
  async fetchSidebarPosts ({ commit }) {
    const { data } = await axios.get(process.env.apiUrl + 'noticias/')
    commit('SET_STARS', data)
  }
}

export const getters = {
  getSidebarPosts: state => state.sidebar.posts
}

export default createStore
